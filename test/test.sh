#
# Copyright 2017 Blockie AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

_TEST_WEB3_ABI_ENCODE_MESSAGE_STRING_HELLO_WORLD()
{
    SPACE_DEP="WEB3_ABI_ENCODE PRINT"

    local output=""
    output=$(WEB3_ABI_ENCODE "$1" "$2" "$3")

    local expected_output="0x05c766d10000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000000b48656c6c6f20776f726c64000000000000000000000000000000000000000000"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

_TEST_WEB3_ABI_ENCODE_FOO_BAZ()
{
    SPACE_DEP="WEB3_ABI_ENCODE PRINT"

    local output=""
    output=$(WEB3_ABI_ENCODE "$1" "$2" "$3")

    local expected_output="0xcdcd77c000000000000000000000000000000000000000000000000000000000000000450000000000000000000000000000000000000000000000000000000000000001"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

_TEST_WEB3_ABI_ENCODE_FOO_SAM_SIMPLE()
{
    SPACE_DEP="WEB3_ABI_ENCODE PRINT"

    local output=""
    output=$(WEB3_ABI_ENCODE "$1" "$2" "$3")

    local expected_output="0x2fd6b0a200000000000000000000000000000000000000000000000000000000000000600000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000046461766500000000000000000000000000000000000000000000000000000000"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

_TEST_WEB3_ABI_ENCODE_FOO_SAM()
{
    SPACE_DEP="WEB3_ABI_ENCODE PRINT"

    local output=""
    output=$(WEB3_ABI_ENCODE "$1" "$2" "$3")

    local expected_output="0xa5643bf20000000000000000000000000000000000000000000000000000000000000060000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000a0000000000000000000000000000000000000000000000000000000000000000464617665000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003000000000000000000000000000000000000000000000000000000000000000100000000000000000000000000000000000000000000000000000000000000020000000000000000000000000000000000000000000000000000000000000003"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

_TEST_WEB3_ABI_ENCODE_F_DYNAMIC()
{
    SPACE_DEP="WEB3_ABI_ENCODE PRINT"

    local output=""
    output=$(WEB3_ABI_ENCODE "$1" "$2" "$3")

    local expected_output="0xd2c4ef3100000000000000000000000000000000000000000000000000000000000001230000000000000000000000000000000000000000000000000000000000000080499602d20000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000e0000000000000000000000000000000000000000000000000000000000000000200000000000000000000000000000000000000000000000000000000000004560000000000000000000000000000000000000000000000000000000000000789000000000000000000000000000000000000000000000000000000000000000b48656c6c6f20776f726c64000000000000000000000000000000000000000000"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}

#
# https://github.com/ethereum/wiki/wiki/JSON-RPC
# eth_getStorageAt example : https://github.com/ethereum/wiki/wiki/JSON-RPC#eth_getstorageat
_TEST_WEB3_CALCULATE_STORAGE_POSITION()
{
    SPACE_DEP="WEB3_GET_STORAGE_ADDRESS PRINT"

    local output=""
    output=$(WEB3_GET_STORAGE_ADDRESS $@)

    # Expected output:
    # keccak(decodeHex("000000000000000000000000391694e7e0b0cce554cb130d723a9d27458f9298" + "0000000000000000000000000000000000000000000000000000000000000001"))
    local expected_output="0x6661e9d6d8b923d5bbaab1b96e1dd51ff6ea2a93520fdc9eb75d059238b8c5e9"

    if [ "${expected_output}" = "${output}" ]; then
        PRINT "OK: success (${output})" "ok"
        exit 0
    else
        PRINT "Failed." "error"
        PRINT "Expected: ${expected_output}." "error"
        PRINT "Got:      ${output}" "error"
        exit 1
    fi
}
