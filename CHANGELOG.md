# Space Module change log - web-sh/web3

## [current]

+ Add local Keccak code path to `/abi/`

* Change `/abi/` to not add line breaks to hexadecimal output

* Change `/abi/` to not output method ID hexadecimal for constructors

* Update `/abi/` to handle address data type separately

* Update `/abi/` integer code path to disconsider whitespaces

* Update `QUOTE_ARG` to consider integers


## [0.1.0 - 2017-12-07]

+ Add license

+ Add `info/title` description to all nodes

+ Add helper tools for development

+ Add tests

+ Add `-e endpoint` to `/abi/`

* Fix various bugs in `dep_install`

* Handle various static analysis warnings

* Update code documentation


## [0.0.1 - 2017-12-06]

+ Initial version
