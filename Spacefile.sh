#
# Copyright 2017 Blockie AB
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


# Disable shellcheck warning about unused out parameters
# shellcheck disable=SC2034

#=====================
# WEB3_DEP_INSTALL
#
# Check dependencies for this module.
#
# Returns:
#   0: dependencies were found
#   1: failed to find dependencies
#
#=====================
WEB3_DEP_INSTALL()
{
    SPACE_DEP="OS_IS_INSTALLED PRINT"       # shellcheck disable=SC2034

    PRINT "Checking for web3 dependencies." "info"

    local out_ostype=''
    local out_ospkgmgr=''
    local out_oshome=''
    local out_oscwd=''
    local out_osinit=''
    OS_ID
    if [ "${out_ospkgmgr}" = "yum" ]; then
        curl -LO "https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64"
        chmod +x "jq-linux64"
        mv -f "jq-linux64" "/usr/local/bin/jq"
    fi

    if [ "${out_ospkgmgr}" != "pacman" ]; then
        if [ "${out_ospkgmgr}" = "apk" ]; then
            if ! OS_IS_INSTALLED "vim" "vim"; then
                PRINT "Failed finding dependencies." "error"
                return 1
            fi
        else
            if ! OS_IS_INSTALLED "vim-common" "vim-common"; then
                PRINT "Failed finding dependencies." "error"
                return 1
            fi
        fi
    fi

    if OS_IS_INSTALLED "curl" "curl" && OS_IS_INSTALLED "cut" "cut" && OS_IS_INSTALLED "jq" "jq" && OS_IS_INSTALLED "sed" "sed" && OS_IS_INSTALLED "xxd" "xxd"; then
        PRINT "Dependencies found." "ok"
    else
        PRINT "Failed finding dependencies." "error"
        return 1
    fi
}

#=====================
# WEB3_ABI_ENCODE
#
# Create ABI encoded data
#
# Parameters:
#   $1: node endpoint address
#   $2: function signature (method)
#   $3: function parameters
#
# Expects:
#   KECCAK: path to keccak-256sum
#
# Returns:
#   Non-zero on failure
#
#=====================
WEB3_ABI_ENCODE()
{
    SPACE_SIGNATURE="endpoint:0 method [parameters]"
    SPACE_DEP="STRING_LPAD STRING_RPAD PRINT WEB3_RPC OS_COMMAND"
    # shellcheck disable=SC2034
    SPACE_ENV="KECCAK=${KECCAK:-keccak-256sum}"

    local endpoint="${1}"
    shift

    local method="${1}"
    shift

    local parameters="${1-}"
    shift

    local data=""
    local method_id=""
    if [ -n "${method}" ]; then

        local method_name=""
        local method_parameters=""
        method_name=$(printf "%s" "${method}" | sed 's/\(.*\)(\(.*\))/\1/g')
        method_parameters=$(printf "%s" "${method}" | sed 's/\(.*\)(\(.*\))/\2/g')
        if [ "${method_name}" = "${method_parameters}" ]; then
            PRINT "Invalid function signature: ${method}" "error"
            return 1
        fi

        if [ "${method_parameters}" != "${method_parameters%[[:space:]]*}" ]; then
            PRINT "Function parameters must not contain spaces: ${method_parameters}" "warning"
            method_parameters=$(printf "%s" "${method_parameters}" | sed "s/[[:space:]]//g")
            method="${method_name}""(${method_parameters})"
            PRINT "Function signature changed to: ${method}" "warning"
        fi

        PRINT "Method name: ${method_name}" "debug"
        PRINT "Method parameters: ${method_parameters}" "debug"

        if OS_COMMAND "${KECCAK}" >/dev/null; then
            method_id="0x"$(printf "${method}" | ${KECCAK} -l)
        elif [ -n "${endpoint}" ]; then
            local method_hex="0x"
            method_hex=${method_hex}$(printf "%s" "${method}" | xxd -pu -c 99999999999)
            method_id=$(WEB3_RPC "${endpoint}" "web3_sha3" "/result" "" "2" "0" "${method_hex}")
            if [ -z "${method_id}" ]; then
                PRINT "Error calling web3_sha3 via WEB3_RPC. Make sure ${endpoint} is accessible" "error"
                return 1
            fi
        else
            PRINT "Could not find keccak (${KECCAK}) locally nor any endpoint defined for web3_sha3." "error"
            return 1
        fi

        #
        # Only add method id if the signature is not constructor
        # The constructor is characterized as unnamed function signature
        if [ -n "${method_name}" ];then
            data="${data}"$(printf "%s" "${method_id}" | cut -c1-10)
        fi
    fi

    if [ "${parameters}" != "" ]; then
        # TODO: handle escape chars, !, etc
        # TODO: handle quoted arguments
        # TODO: handle arguments with comma

        local offset=0
        local argc=0

        local parameter_type=""
        for parameter_type in $(printf "%s" "${method_parameters}" | sed "s/,/ /g"); do
            argc=$((argc + 1))
        done

        offset=$((offset + 32 * argc))
        PRINT "offset head part: ${offset}" "debug"

        local dynamic_data=""
        local parameter_type_index=0
        parameter_type=""
        for parameter_type in $(printf "%s" "${method_parameters}" | sed "s/,/ /g"); do
            PRINT "Processing parameter: ${parameters} (${parameter_type})" "debug"

            local _index=0
            local current_param=""
            local is_array=false
            local composed_param=""
            IFS=","
            for current_param in ${parameters}; do
                PRINT "current_param: $current_param" "debug"
                PRINT "composed param: $composed_param" "debug"
                current_param=$(printf "%s" "${current_param}" | sed "s/^[[:space:]]*//g")

                if [ "${parameter_type_index}" -eq "$_index" ]; then
                    PRINT "selected param: $current_param" "debug"

                    local current_param_left_bracket=
                    local current_param_right_bracket=
                    current_param_left_bracket=$(printf "%s" "${current_param}" | sed "s/\[//")
                    current_param_right_bracket=$(printf "%s" "${current_param}" | sed "s/\]//")

                    # FIXME: single unit case: [0]
                    if [ "${current_param_left_bracket}" != "${current_param}" ]; then
                        is_array=true
                        composed_param=$current_param
                        continue 1
                    elif [ "${current_param_right_bracket}" != "${current_param}" ]; then
                        is_array=false
                        composed_param=${composed_param}","$current_param
                        current_param=${composed_param}
                        parameter_type_index=$((parameter_type_index + 1))
                        _index=$((_index + 1))
                        break 1
                    elif [ "${is_array}" = true ]; then
                        composed_param=${composed_param}","$current_param
                        continue 1
                    fi
                    break 1
                fi
                _index=$((_index + 1))
            done
            unset IFS
            parameter_type_index=$((parameter_type_index + 1))

            #
            # Fixed data types
            #
            if [ "${parameter_type}" = "uint" ]         \
                || [ "${parameter_type}" = "uint8" ]    \
                || [ "${parameter_type}" = "uint16" ]   \
                || [ "${parameter_type}" = "uint32" ]   \
                || [ "${parameter_type}" = "uint128" ]  \
                || [ "${parameter_type}" = "uint256" ]  \
                || [ "${parameter_type}" = "int" ]      \
                || [ "${parameter_type}" = "int8" ]     \
                || [ "${parameter_type}" = "int16" ]    \
                || [ "${parameter_type}" = "int32" ]    \
                || [ "${parameter_type}" = "int128" ]   \
                || [ "${parameter_type}" = "int256" ]; then
                current_param=$(printf "%s" "${current_param}" | tr -d "[:space:]")
                PRINT "Appending hexadecimal for: ${current_param}" "debug"
                data_to_append=$(printf "%02x" "${current_param}")
                STRING_LPAD "data_to_append" "0" "64"
                PRINT "Data to append: ${data_to_append}" "debug"
                data="${data}"$(printf "%s" "$data_to_append")

            elif [ "${parameter_type}" = "address" ]; then
                PRINT "Appending hexadecimal for: ${current_param}" "debug"
                data_to_append=$(printf "%s" "${current_param#0x}" | tr '[:upper:]' '[:lower:]')
                STRING_LPAD "data_to_append" "0" "64"
                PRINT "Data to append: ${data_to_append}" "debug"
                data="${data}"$(printf "%s" "$data_to_append")

            elif [ "${parameter_type}" = "bytes10" ]; then
                PRINT "bytes10 appending: ${current_param}" "debug"
                data_to_append=$(printf "%02x" "${current_param}")
                STRING_RPAD "data_to_append" "0" "64"
                data="${data}"$(printf "%s" "$data_to_append")

            elif [ "${parameter_type}" = "bool" ]; then
                PRINT "bool" "debug"

                if [ "${current_param}" = "true" ]; then
                    data_to_append=$(printf "%02x" 1)
                elif [ "${current_param}" = "false" ]; then
                    data_to_append=$(printf "%02x" 0)
                else
                    PRINT "Unknown boolean value: ${current_param}" "error"
                    return 1
                fi

                STRING_LPAD "data_to_append" "0" "64"
                data="${data}"$(printf "%s" "$data_to_append")

            elif [ "${parameter_type}" = "fixed128x19" ]    \
                || [ "${parameter_type}" = "ufixed128x19" ]; then

                # TODO: add custom fixed-point types MxN
                PRINT "Fixed point types are currently not supported" "error"
                return 1

            elif [ "${parameter_type}" = "function" ]; then
                PRINT "function" "debug"

            #
            # Dynamic data types
            #
            elif [ "${parameter_type}" = "bytes" ]      \
                || [ "${parameter_type}" = "string" ]; then
                # TODO: add custom bytes1, bytes2, ..., bytes32
                PRINT "bytes or string" "debug"

                #
                # string
                # TODO: utf8
                _string="${current_param}"
                _string_len=${#_string}
                _string_hex=""
                _string_len_hex=$(printf "%02x" "${_string_len}")
                PRINT "string argument length: ${_string_len} (${_string_len_hex})" "debug"
                PRINT "string argument: ${_string}" "debug"

                # FIXME: expand to N-byte strings
                local _counter=0
                while [ "${_counter}" -lt 32 ]; do
                    _current_char=$(printf "%s" "${_string}" | cut -c"$((_counter+1))")
                    if [ "${_current_char}" = " " ]; then
                        _string_hex=${_string_hex}$(printf "20");
                    elif [ -z "${_current_char}" ]; then
                        _string_hex=${_string_hex}$(printf "00");
                    else
                        _string_hex=${_string_hex}$(printf "%02x" \'"${_current_char}");
                    fi
                    _counter=$((_counter + 1))
                done

                PRINT "string argument converted to hex: ${_string_hex}" "debug"
                # Offset to argument
                PRINT "Adding offset: ${offset}" "debug"
                data_to_append=$(printf "%02x" ${offset})
                STRING_LPAD "data_to_append" "0" "64"
                data="${data}"$(printf "%s" "$data_to_append")

                # String parameter length (bytes)
                length_to_append="${_string_len_hex}"
                STRING_LPAD "length_to_append" "0" "64"
                data_to_append=${length_to_append}

                # String parameter padded to 32 bytes
                data_to_append=${data_to_append}${_string_hex}
                dynamic_data="${dynamic_data}"$(printf "%s" "$data_to_append")

                # Offset to start of dynamic parameter
                offset=$((offset + 32*2))
                PRINT "bytes/string Offset now: ${offset}" "debug"

            elif [ "${parameter_type}" = "uint32[]" ] \
                || [ "${parameter_type}" = "uint256[]" ]; then
                PRINT "uint*[]" "debug"

                #
                _string="${current_param}"
                PRINT "string: ${_string}" "debug"

                _string_len=0
                local counter=0
                for counter in $(printf "%s" "${_string}" | sed "s/,/ /g"); do
                    PRINT "accumulating length: ${counter}" "debug"
                    _string_len=$((_string_len + 1))
                done
                _string_hex=""
                _string_len_hex=$(printf "%02x" "${_string_len}")
                PRINT "string argument length: ${_string_len} (${_string_len_hex})" "debug"
                PRINT "string argument: ${_string}" "debug"

                PRINT "string argument converted to hex: ${_string_hex}" "debug"
                # Offset to argument
                PRINT "Adding offset: ${offset}" "debug"
                data_to_append=$(printf "%02x" ${offset})
                STRING_LPAD "data_to_append" "0" "64"
                data="${data}"$(printf "%s" "$data_to_append")

                # String parameter length (bytes)
                length_to_append="${_string_len_hex}"
                STRING_LPAD "length_to_append" "0" "64"
                data_to_append=${length_to_append}

                local number=
                _string=$(printf "%s" "${_string}" | sed -e "s/\[//" -e "s/\]//")
                PRINT "uint[] string now: $_string" "debug"
                for number in $(printf "%s" "${_string}" | sed "s/,/ /g"); do
                    PRINT "number: ${number}" "debug"
                    number=$(printf "%02x" "${number}")
                    STRING_LPAD "number" "0" "64"
                    PRINT "padded number: ${number}" "debug"
                    PRINT "data now: ${data_to_append}"
                    data_to_append=${data_to_append}${number}
                    PRINT "appended data: ${data_to_append}"
                    offset=$((offset + 32))
                done
                dynamic_data="${dynamic_data}"$(printf "%s" "$data_to_append")

                # Offset to start of dynamic parameter
                offset=$((offset + 32))
                PRINT "(uint[] Offset now: ${offset}" "debug"

            elif [ "${parameter_type}" = "*[]" ]; then
                # TODO: add custom dynamic array
                PRINT "Custom arrays are currently not supported" "error"
                return 1

            elif [ "${parameter_type}" = "*[i]" ]; then
                # TODO: add custom dynamic array
                PRINT "Custom arrays are currently not supported" "error"
                return 1

            else
                PRINT "Unsupported data type: ${parameter_type}" "error"

            fi
        done

        # Append dynamic data
        data="${data}"$(printf "%s" "$dynamic_data")

    fi
    printf "%s\n" "${data}"
}

#=====================
# WEB3_RPC
#
# Main function for handling RPC calls
#
# Parameters:
#   $1: node endpoint address
#   $2: RPC method to call
#   $3: path
#   $4: convert extracted method
#   $5: identification
#   $6: raw arguments flag
#   $7: parameters
#
# Returns:
#   Non-zero on failure
#
#=====================
WEB3_RPC()
{
    SPACE_SIGNATURE="endpoint:0 method path:0 convert:0 id:0 israwargs:0 [params]"
    SPACE_DEP="_WEB3_RUN_COMMAND _WEB3_QUOTE_ARG"

    local endpoint="${1}"
    shift

    local method="${1}"
    shift

    local path="${1-}"
    shift

    local convert="${1-}"
    shift

    local id="${1:-1}"
    shift

    local israwargs="${1:-0}"
    shift

    local params=""

    if [ $# -gt 0 ]; then
        local arg=
        local argkey=
        local argvalue=
        local jsonargs=
        local index=0
        local islastarg=0
        for arg in "$@"; do
            index=$((index+1))
            if [ "${israwargs}" = 0 ]; then
                # Check if it might be a JSON argument
                argkey="${arg%%:*}"
                if [ "${argkey}" != "${arg}" ]; then
                    argvalue="${arg#*:}"
                    if [ -n "${argkey}" ]; then
                        if [ -n "${argvalue}" ]; then
                            _WEB3_QUOTE_ARG "argvalue"
                            jsonargs="${jsonargs:+${jsonargs}, }\"${argkey}\": ${argvalue}"
                        fi
                    fi
                    # continue unless is last argument.
                    if [ ${index} -lt $# ]; then
                        continue
                    fi
                    islastarg=1
                fi
            fi
            if [ -n "${jsonargs}" ]; then
                params="${params:+${params}, }{${jsonargs}}"
                jsonargs=
            fi

            if [ ${islastarg} -eq 1 ]; then
                break
            fi

            _WEB3_QUOTE_ARG "arg"
            params="${params:+${params}, }${arg}"
        done
    fi
    _WEB3_RUN_COMMAND "${endpoint}" "${method}" "${params}" "${id}" "${path}" "${convert}"
}

#=====================
# WEB3_GET_STORAGE_ADDRESS
#
# Calculates the storage position of a variable in a contract
#
# Parameters:
#   $1: node endpoint address
#   $2: positions
#
# Expects:
#   KECCAK: path to keccak-256sum
#
# Returns:
#   Non-zero on failure
#
#=====================
WEB3_GET_STORAGE_ADDRESS()
{
    SPACE_SIGNATURE="endpoint:0 positions"
    SPACE_DEP="STRING_LPAD WEB3_RPC OS_COMMAND"
    SPACE_ENV="KECCAK=${KECCAK:-keccak-256sum}"

    local endpoint="${1}"
    shift

    local hex="0x"
    local arg=

    for arg in "$@"; do
        arg="${arg#0x}"
        STRING_LPAD "arg" "0" 64
        hex="${hex}${arg}"
    done

    PRINT "Storage key: ${hex}" "debug"
    if OS_COMMAND ${KECCAK} >/dev/null; then
        local hex_output=""
        hex_output="0x"$(printf "${hex#0x}" | ${KECCAK} -x -l | cut -c1-64)
        printf "%s\n" "${hex_output}"
    elif [ -n "${endpoint}" ]; then
        WEB3_RPC "${endpoint}" "web3_sha3" "/result" "" "2" "0" "${hex}"
    else
        PRINT "Could not find keccak (${KECCAK}) locally nor any endpoint defined for web3_sha3." "error"
        return 1
    fi

}

#=====================
# WEB3_RUN_COMMAND
#
# Backend implementation for handling the actual RPC call
#
# Parameters:
#   $1: node endpoint address
#   $2: method
#   $3: parameters
#   $4: identification
#   $5: path
#   $6: convert extracted method
#
# Returns:
#   Non-zero on failure
#
#=====================
_WEB3_RUN_COMMAND()
{
    SPACE_SIGNATURE="endpoint:0 method params:0 id [path convert]"
    SPACE_DEP="PRINT _WEB3_JSON_EXTRACT"

    local endpoint="${1:-"http://127.0.0.1:8545"}"
    shift

    local method="${1}"
    shift

    local params="${1}"
    shift

    local id="${1}"
    shift

    local path="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    local convert="${1-}"
    shift $(( $# > 0 ? 1 : 0 ))

    PRINT "JSON path to extract: ${path}" "debug"

    local cmd=
    cmd=$(printf '{"jsonrpc":"2.0","method":"%s", "params":[%s], "id":%s}' "${method}" "${params}" "${id}")
    PRINT "curl -s ${endpoint} -X POST -H 'Content-Type: application/json' --data ${cmd}" "debug"

    local output=
    output=$(curl -s "${endpoint}" -X POST -H 'Content-Type: application/json' --data "${cmd}")

    local status="$?"
    if [ ${status} -ne 0 ]; then
        return ${status}
    fi

    # Detect error mesage
    local error=
    if ! error=$(_WEB3_JSON_EXTRACT "${output}" "/error/message"); then
        PRINT "Could not extract from JSON" "error"
        return 1
    elif [ -n "${error}" ] && [ "${error}" != "null" ]; then
        PRINT "${error}" "error"
        return 1
    fi

    if [ -n "${path}" ]; then
        if ! output=$(_WEB3_JSON_EXTRACT "${output}" "${path}"); then
            PRINT "Could not extract from JSON" "error"
            return 1
        fi
        if [ "${convert}" = "dec" ]; then
            if [ "${output}" = "true" ]; then
                output="1"
            elif [ "${output}" = "false" ]; then
                output="0"
            else
                if ! output=$(printf "%d" "${output}"); then
                    return 1
                fi
            fi
        elif [ -n "${convert}" ]; then
            PRINT "Unknown convert: ${convert}" "error"
            return 1
        fi
    fi
    printf "%s\n" "${output}"
}

#=====================
# WEB3_JSON_EXTRACT
#
# Handles JSON data reading
#
# Parameters:
#   $1: json file
#   $2: path
#
# Returns:
#   Non-zero on failure
#
#=====================
_WEB3_JSON_EXTRACT()
{
    SPACE_SIGNATURE="json path"
    SPACE_DEP="OS_COMMAND PRINT STRING_SUBST"

    local json="${1}"
    shift

    local path="${1}"
    shift

    # Convert path to jq format
    STRING_SUBST "path" "/" "." "1"
    # Adjust for arrays
    STRING_SUBST "path" ".[" "["

    if ! OS_COMMAND jq >/dev/null; then
        PRINT "Could not find jq command to extract from json." "error"
        return 1
    fi

    printf "%s\n" "${json}" | jq -r "${path}" 2>/dev/null
}

#=====================
# WEB3_QUOTE_ARG
#
# Helper function for escaping and quoting arguments in place
#
# Parameters:
#   $1: variable to quote
#
# Returns:
#   Non-zero on failure
#
#=====================
_WEB3_QUOTE_ARG()
{
    # shellcheck disable=SC2034
    SPACE_SIGNATURE="variable"
    # shellcheck disable=SC2034
    SPACE_DEP="STRING_ESCAPE"

    local _var=
    eval "_var=\"\${${1}}\""

    if [ "${_var#\[}" != "${_var}" ]; then
        # Array, use sed to quote every hex string
        _var=$(printf "%s\n" "${_var}" | sed 's/\(0x[a-zA-Z0-9]*\)/"&"/g')
    elif [ "${_var#\{}" != "${_var}" ]; then
        # Object, do nothing
        :
    elif [ "${_var}" = "true" ]; then
        # Boolean, do nothing
        :
    elif [ "${_var}" = "false" ]; then
        # Boolean, do nothing
        :
    elif [ "${_var}" = "null" ]; then
        # Null, do nothing
        :
    elif [ "$(printf "%d" "${_var}" 2>/dev/null)" = "${_var}" ]; then
        # Integer, do nothing
        :
    elif [ "${_var#\"}" = "${_var}" ]; then
        # Unquoted string or hex value, quote it
        STRING_ESCAPE "_var" '"'
        _var="\"${_var}\""
    fi

    eval "${1}=\"\${_var}\""
}
