#!/usr/bin/env sh
#
# Copyright 2017 Blockie AB
#
# This file is part of web3-sh/web3
#
# web3-sh/web3 is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 3 of the License.
#
# web3-sh/web3 is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with web3-sh/web3  If not, see <http://www.gnu.org/licenses/>.
#

#
# Bump web3 version
#

set -o errexit
set -o nounset


#
# Check arguments
if [ "$#" -lt 1 ]; then
    printf "Missing version name. Example: $0 1.0.0\n" >&2
    exit 1
fi

#
# Check requirements
if ! command -v sed >/dev/null; then
    printf "Missing sed program. Exiting...\n" >&2
    exit 1
fi

#
# Files to change
_changelog_file="./CHANGELOG.md"
_version_file="./space-module.txt"

#
# Check files are present either on current or parent directory
if [ ! -f "$_changelog_file" ]; then
    _changelog_file="./../CHANGELOG.md"
    if [ ! -f "$_changelog_file" ]; then
        printf "Unable to find file: %s\n" "${_changelog_file}" >&2
        exit 1
    fi
fi
if [ ! -f "$_version_file" ]; then
    _version_file="./../space-module.txt"
    if [ ! -f "$_version_file" ]; then
        printf "Unable to find file: %s\n" "${_version_file}" >&2
        exit 1
    fi
fi

#
# Data
_version_name="$1"
_version_date=$(date "+%Y-%m-%d")

# Update files
printf "Updating file: %s\n" "${_changelog_file}"
sed -i.bak "s/\[current\]/\[${_version_name} - ${_version_date}\]/" "$_changelog_file"
printf "Updating file: %s\n" "${_version_file}"
sed -i.bak "s/Space v1: .*/Space v1: ${_version_name}/g" "$_version_file"

#
# Cleanup temporary files
if [ -f "$_changelog_file" ]; then
    rm "${_changelog_file}.bak"
fi

if [ -f "$_version_file" ]; then
    rm "${_version_file}.bak"
fi
