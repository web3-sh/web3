#!/usr/bin/env sh
#
# Copyright 2017 Blockie AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

_spacefile_name="Spacefile.sh"
_spacefile_path="./${_spacefile_name}"
_output_path="./README.md"

#
# Check files are present either on current or parent directory
if [ ! -f "${_spacefile_path}" ]; then
    printf "Failed to load file: %s\n" "${_spacefile_path}" >&2
    exit 1
fi

space -m autodoc /export/ -- "${_spacefile_path}"       \
    && mv "${_spacefile_name}"_README "${_output_path}" \
    && printf "README.md has been overwritten\n"
