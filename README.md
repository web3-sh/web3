#  | [![build status](https://gitlab.com/space-sh/web3/badges/master/build.svg)](https://gitlab.com/space-sh/web3/commits/master)


## /abi/
	Create ABI encoded data


## /convert/
	

+ todec
+ tohex

## /eth_accounts/
	Returns a list of addresses

+ result

## /eth_blocknumber/
	Returns the latest block number

+ result

## /eth_call/
	Perform a new message call

+ result

## /eth_coinbase/
	Returns the client coinbase address

+ result

## /eth_compilelll/
	Returns compiled LLL code

+ result

## /eth_compileserpent/
	Returns compiled Serpent code

+ result

## /eth_compilesolidity/
	Returns compiled Solidity code

+ result

## /eth_estimategas/
	Perform a new message call and return the used gas

+ result

## /eth_gasprice/
	Returns the current price per gas in wei

+ result

## /eth_getbalance/
	Returns the balance of a given account address

+ result

## /eth_getblockbyhash/
	Returns information about a block given a block hash

+ result

## /eth_getblockbynumber/
	Returns information about a block given a block number

+ result

## /eth_getblocktransactioncountbyhash/
	Returns the number of transactions in a specific block hash

+ result

## /eth_getblocktransactioncountbynumber/
	Returns the number of transactions in a specific block number

+ result

## /eth_getcode/
	Returns code at a given address

+ result

## /eth_getcompilers/
	Returns a list of available compilers

+ result

## /eth_getfilterchanges/
	Watch for filter changes using a polling mechanism

+ result

## /eth_getfilterlogs/
	Returns an array containing all logs matching a filter id

+ result

## /eth_getlogs/
	Returns an array containing all logs matching a filter object

+ result

## /eth_getstorageat/
	Returns the value from a storage position at a given address

+ result

## /eth_gettransactionbyblockhashandindex/
	Returns information about a transaction given a block hash and transaction index

+ result

## /eth_gettransactionbyblocknumberandindex/
	Returns information about a transaction given a block number and transaction index

+ result

## /eth_gettransactionbyhash/
	Returns information about a transaction given a transaction hash

+ result

## /eth_gettransactioncount/
	Returns the number of transactions originated from a given address

+ result

## /eth_gettransactionreceipt/
	Returns the transaction receipt given a transaction hash

+ result

## /eth_getunclebyblockhashandindex/
	Returns information about an uncle given a block hash and uncle index

+ result

## /eth_getunclebyblocknumberandindex/
	Returns information about an uncle given a block number and uncle index

+ result

## /eth_getunclecountbyblockhash/
	Returns the number of uncles in a specific block hash

+ result

## /eth_getunclecountbyblocknumber/
	Returns the number of uncles in a specific block number

+ result

## /eth_getwork/
	Returns the hash of the current block, the seed hash and the target condition

+ result

## /eth_hashrate/
	Returns the mining hash rate in seconds

+ result

## /eth_mining/
	Returns boolean state representing whether the client is mining new blocks

+ result

## /eth_newblockfilter/
	Create a new block filter

+ result

## /eth_newfilter/
	Create a new filter

+ result

## /eth_newpendingtransactionfilter/
	Create a new transaction filter

+ result

## /eth_protocolversion/
	Returns the current Ethereum protocol version

+ result

## /eth_sendrawtransaction/
	Perform a new signed message call transaction

+ result

## /eth_sendtransaction/
	Perform a new message call transaction

+ result

## /eth_sign/
	Sign data message with a given account address

+ result

## /eth_submithashrate/
	Submit the mining hash rate

+ result

## /eth_submitwork/
	Submit a Proof-of-Work solution

+ result

## /eth_syncing/
	Returns an object containing synchronization status

+ result

## /eth_uninstallfilter/
	Remove an existing filter

+ result

## /getstorageaddress/
	Calculate the storage position of a variable in a contract

	Apply arbitrary number of arguments.
	


## /net_listening/
	Returns boolean state representing whether the client is listening for network connections

+ result

## /net_peercount/
	Returns number of peers connected to the client

+ result

## /net_version/
	Returns the current network protocol version

+ result

## /shh_getfilterchanges/
	Watch for whisper filter changes using a polling mechanism

+ result

## /shh_getmessages/
	Get all whisper messages that match a particular filter

+ result

## /shh_hasidentity/
	Checks if the client holds the private keys for a given identity

+ result

## /shh_newfilter/
	Create a new whisper filter

+ result

## /shh_newidentity/
	Creates new whisper identity

+ result

## /shh_post/
	Sends a whisper message

+ result

## /shh_uninstallfilter/
	Removes an existing whisper filter

+ result

## /shh_version/
	Returns the current whisper protocol version

+ result

## /web3_clientversion/
	Returns the current client version

+ result

## /web3_sha3/
	Returns the Keccak-256 of a given data

+ result

# Functions 

## WEB3\_DEP\_INSTALL()  
  
WEB3  
  
Check dependencies for this module.  
  
### Returns:  
- 0: dependencies were found  
- 1: failed to find dependencies  
  
  
  
## WEB3\_ABI\_ENCODE()  
  
WEB3  
  
Create ABI encoded data  
  
### Parameters:  
- $1: node endpoint address  
- $2: function signature (method)  
- $3: function parameters  
  
### Expects:  
- KECCAK: path to keccak-256sum  
  
### Returns:  
- Non-zero on failure  
  
  
  
## WEB3\_RPC()  
  
WEB3  
  
Main function for handling RPC calls  
  
### Parameters:  
- $1: node endpoint address  
- $2: RPC method to call  
- $3: path  
- $4: convert extracted method  
- $5: identification  
- $6: raw arguments flag  
- $7: parameters  
  
### Returns:  
- Non-zero on failure  
  
  
  
## WEB3\_GET\_STORAGE\_ADDRESS()  
  
WEB3  
  
Calculates the storage position of a variable in a contract  
  
### Parameters:  
- $1: node endpoint address  
- $2: positions  
  
### Expects:  
- KECCAK: path to keccak-256sum  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_WEB3\_RUN\_COMMAND()  
  
WEB3  
  
Backend implementation for handling the actual RPC call  
  
### Parameters:  
- $1: node endpoint address  
- $2: method  
- $3: parameters  
- $4: identification  
- $5: path  
- $6: convert extracted method  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_WEB3\_JSON\_EXTRACT()  
  
WEB3  
  
Handles JSON data reading  
  
### Parameters:  
- $1: json file  
- $2: path  
  
### Returns:  
- Non-zero on failure  
  
  
  
## \_WEB3\_QUOTE\_ARG()  
  
WEB3  
  
Helper function for escaping and quoting arguments in place  
  
### Parameters:  
- $1: variable to quote  
  
### Returns:  
- Non-zero on failure  
  
  
  
